package testPackage;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import conversorDeNumeroRomano.ConversorNumeroRomano;

public class ConversorNumerosRomanosTest {

	public ConversorNumerosRomanosTest() {
		
	}
	
	@Test
	public void deveEntenderOSimboloI() {
		ConversorNumeroRomano conversor = new ConversorNumeroRomano();
		int retorno = conversor.converter("I");
		assertEquals(retorno, 1);
	}
	
	@Test
	public void deveEntenderOSimboloV() {
		ConversorNumeroRomano conversor = new ConversorNumeroRomano();
		int retorno = conversor.converter("V");
		assertEquals(retorno, 5);
	}
	
	@Test
	public void deveEntenderOSimboloX() {
		ConversorNumeroRomano conversor = new ConversorNumeroRomano();
		int retorno = conversor.converter("X");
		assertEquals(retorno, 10);
	}
	
	@Test
	public void deveEntenderOSimboloL() {
		ConversorNumeroRomano conversor = new ConversorNumeroRomano();
		int retorno = conversor.converter("L");
		assertEquals(retorno, 50);
	}
	
	@Test
	public void deveEntenderOSimboloC() {
		ConversorNumeroRomano conversor = new ConversorNumeroRomano();
		int retorno = conversor.converter("C");
		assertEquals(retorno, 100);
	}

	@Test
	public void deveEntenderOSimboloD() {
		ConversorNumeroRomano conversor = new ConversorNumeroRomano();
		int retorno = conversor.converter("D");
		assertEquals(retorno, 500);
	}
	
	@Test
	public void deveEntenderOSimboloM() {
		ConversorNumeroRomano conversor = new ConversorNumeroRomano();
		int retorno = conversor.converter("M");
		assertEquals(retorno, 1000);
	}
	
	@Test
	public void deveEntenderOSimboloII() {
		ConversorNumeroRomano conversor = new ConversorNumeroRomano();
		int retorno = conversor.converter("II");
		assertEquals(retorno, 2);
	}
}
