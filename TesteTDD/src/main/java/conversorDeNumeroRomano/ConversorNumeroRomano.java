package conversorDeNumeroRomano;

import java.util.HashMap;
import java.util.Map;

public class ConversorNumeroRomano {

	private static Map<Character, Integer> tabelaDeNumeros = new HashMap<Character, Integer>() {
		{
			put('I', 1);
			put('V', 5);
			put('X', 10);
			put('L', 50);
			put('C', 100);
			put('D', 500);
			put('M', 1000);
		}
	};

	public int converter(String numeroRomano) {
		int soma = 0;
		for (int i = 0; i < numeroRomano.length(); i++) {
			soma += tabelaDeNumeros.get(numeroRomano.charAt(i));
		}
		return soma;
	}

}
